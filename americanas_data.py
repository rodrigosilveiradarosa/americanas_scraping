from datetime import datetime
from re import compile, search, sub
from scrapy.linkextractors import LinkExtractor
from js2xml import jsonlike, parse
import scrapy
import utilities as util
import json

class AmericanasSpider(scrapy.Spider):
    name = 'americanas'
    allowed_domains = ['americanas.com.br']

    def start_requests(self):
        mode =  self.settings.get('MODE')
        with open('urls.json', 'r') as f:
            urls = json.load(f)
        if mode == 'all':
            for url in urls:
                yield scrapy.Request(url=url['url'], callback=self.parse_item)
        elif mode == 'basic':
            for url in urls:
                yield scrapy.Request(url=url['url'], callback=self.parse_item_basic)
        else:
            print('\nWRONG MODE PROVIDED AS ARGUMENT (-s MODE = <basic> or <all>)\n')

    def get_price(self, response):
        price = response.css('p.sales-price::text').extract_first()
        if price:
            price = price.replace('R$ ', '').replace('.', '').replace(',', '.')
            return float(price)

    def get_name(self, response):
        return response.css('.product-name::text').extract_first()

    def get_url(self, response):
        return response.xpath("//head/link[@rel='canonical']/@href").extract_first()

    def get_price_in_installments(self, response):
        price_in_installments = response.xpath("//p/span/span").extract_first()
        if price_in_installments:
            price_in_installments = price_in_installments.replace('.', '').replace(',', '.')
            list_price_in_installments = compile(util.regex_parse_installments_price).split(price_in_installments)
            list_price_in_installments = list(filter(None, list_price_in_installments))
            if len(list_price_in_installments)>1:
                num_installments = int(list_price_in_installments[0])
                installment_price = float(list_price_in_installments[-1])
                return {'numInstallments': num_installments, 'installmentPrice': installment_price}

    def get_availability(self, response):
        if response.css('div.unavailable-informer > h2::text').extract_first():
            return False
        else:
            return True

    def get_rating_from_json(self, json, response):
        if 'rating' in json['product'].keys():
            if 'average' in json['product']['rating'].keys():
                return int(json['product']['rating']['average'])
            else:
                return self.get_rating(response)

    def get_image_from_json(self, json):
        if 'big' in json['product']['images'][0]:
            return json['product']['images'][0]['big']
        else:
            return json['product']['images'][0]['large']

    def get_number_of_reviews_from_json(self, json):
        if 'rating' in json['product'].keys():
            if 'reviews' in json['product']['rating'].keys():
                return int(json['product']['rating']['reviews'])

    def parse_item(self, response):
        script = response.xpath("//html/head/script[2]/text()").extract()
        js = parse(script[-1])
        js_as_json = jsonlike.getall(js).pop()

        data = {
            'name':                 self.get_name(response),
            'store':                'Americanas',
            'salesPrice':           self.get_price(response),
            'installmentsPrice':    self.get_price_in_installments(response),
            'availability':         self.get_availability(response),
            'image':                self.get_image_from_json(js_as_json),
            'timeStamp':            str(datetime.utcnow()),
            'url':                  self.get_url(response),
            'rating':               self.get_rating_from_json(js_as_json, response),
            'numberOfReviews':      self.get_number_of_reviews_from_json(js_as_json)
        }
        yield data

    def parse_item_basic(self, response):
        data = {
            'salesPrice':           self.get_price(response),
            'installmentsPrice':    self.get_price_in_installments(response),
            'timeStamp':            str(datetime.utcnow()),
            'url':                  self.get_url(response),
        }
        yield data
