# americanas_scraping

This is a part of a bigger platform I developed to the last company I interned on.
It consists on a data scraping solution to extract data of all the products being sold on the biggest Brazilian online store, "Americanas".
I adapted it to work out of a project, and there are basically two standalone Scrapy Spiders: one for collecting urls of products, another to collect the data of the products at that urls.

First, install required libraries with
-pip install -r requirements.txt

To run:
Linux users:
On shell type "./run_urls_collector.sh"
The urls are stored as "urls.json"
After urls are collected, on shell type "./run_data_collector.sh"
The data collected ios stored as "data.json"

Windows users:
Just click on "run urls collector.bat" and type ctrl+c when it`s enough.
The urls are stored as "urls.json"
After urls are collected, click on "run data collector.bat"
The data collected is stored as "data.json"
