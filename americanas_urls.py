from datetime import datetime
from scrapy.linkextractors import LinkExtractor
import scrapy

class AmericanasSpider(scrapy.Spider):
    name = 'americanas'
    allowed_domains = ['americanas.com.br']

    cat_le = LinkExtractor(restrict_css=("nav[id='h_menu']"), allow=('.*americanas.com.br/categoria/.*'))
    subcat_le = LinkExtractor(restrict_css=("div[id='collapse-categoria']"), allow=('.*americanas.com.br/categoria/.*/.*'))
    nav_le = LinkExtractor(restrict_css=("div[class='card card-pagination']"), allow=('.*americanas.com.br/categoria/.*/.*'))
    products_le = LinkExtractor(restrict_xpaths=("//*[@id='content-middle']"), allow=('.*/produto/.*'))
    mapa_do_site_le = LinkExtractor(restrict_css=("ul[class='sitemap-list']"), allow=('.*americanas.com.br/categoria/.*/.*'))

    def start_requests(self):
        url = 	'https://www.americanas.com.br/mapa-do-site'
        yield scrapy.Request(url=url, callback=self.parse_mapa_do_site)

    def parse_general(self, response):
        product_links = self.products_le.extract_links(response)
        nav_links = self.nav_le.extract_links(response)
        for link in product_links:
            data = {
            'timeStamp':        str(datetime.utcnow()),
            'url':              link.url.split('?')[0],
            }
            yield data
        for link in nav_links:
            yield scrapy.Request(url=link.url, callback=self.parse_general)

    def parse_subcat(self, response):
        subcat_links = self.subcat_le.extract_links(response)
        for link in subcat_links:
            yield scrapy.Request(url=link.url, callback=self.parse_general)

    def parse_cat(self, response):
        cat_links = self.cat_le.extract_links(response)
        for link in cat_links:
            yield scrapy.Request(url=link.url, callback=self.parse_subcat)

    def parse_mapa_do_site(self, response):
        mapa_do_site_links = self.mapa_do_site_le.extract_links(response)
        for link in mapa_do_site_links:
            yield scrapy.Request(url=link.url, callback=self.parse_general)
